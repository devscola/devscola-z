En devscola consideramos enriquecedor la colaboración con empresas locales que actuen como referentes para los miembros de la comunidad.
Por eso invitamos a las empresas a participar en sesiones abiertas donde mostrar las herramientas que utilizan a diario o su cultura. Igualmente devscola también ofrece sesiones a los que este interesados en conocer la filosofia de la escuela.

Ejemplo de sesiones que proponemos:
- Herramientas de programación: Frameworks, librerias, lenguajes, patrones, etc.
- Cultura de empresa: metodologia, procesos, proceso de contratación.

