# Devscola Z

## Presentación

Devscola es un proyecto que durante 5 años y sustentada enteramente por voluntarios ha crecido y creado una comunidad cohesionada de aprendices. Muchos de estos aprendices están trabajando en reconocidas empresas tecnológicas en Valencia.

Partiendo de este éxito capacitando como programadores a personas fuera del ámbito tecnológico hemos decidido desarrollar un modelo de cursos donde también participen empresas del sector. Hemos observado que las empresas que ya emplean a miembros de Devscola valoran unas cualidades comunes en sus empleados: autonomía, aprendizaje continuo, responsabilidad, espíritu crítico, sensibilidad por la comunidad local. La idea es aliarnos con las empresas alineadas en estos valores con un objetivo: ofrecer mejor formación a más personas.

Y como vamos a hacerlo?

Desde Devscola propondremos unos modelos de cursos. Cada uno con una duración, temario, miembros involucrados y recursos necesarios. La idea es compartir los cursos con empresas interesadas en patrocinarlos para que entre todos consigamos una formación de mayor calidad.
La forma más directa de patrocinar es con aportaciones económicas. También se puede contribuir con recursos de lo más variado: charlas de profesionales, impartir temarios, libros, cursos online, dotación de espacio, etc.
Por cada uno de los cursos se abrirá una convocatoria de patrocinadores. Si se llegan a cubrir los costes del curso, este se celebrará.
Ejemplo de curso: duración 2 meses, 10 alumnos, 2 mentores, aplicaciones web con ruby, 6.000 euros de presupuesto.

En este momento estamos en fase de búsqueda de patrocinadores interesados para conocer la aceptación de la iniciativa y empezar con la organización del primer curso.

## Presentación 2

### Devscola es una comunidad de aprendizaje de desarrolladores entusiastas de las buenas prácticas.

Contamos ya con 5 años de andadura fomentando **la autonomía, el aprendizaje continuo, el espíritu crítico y la sensibilidad por la comunidad local** de todas aquellas personas que se quieren acercar al mundo de la programación.

Desde la comunidad organizamos eventos gratuitos para compartir prácticas entre desarrolladores: [un evento semanal de práctica deliberada](https://www.meetup.com/Aprende-a-programar-en-Valencia/events/cjrfsqyzpbjc/), [masterclases](https://www.youtube.com/playlist?list=PL8pfR1_uyqPpyMZeH9V0dZ66-LlV0xOBy), [cursos para aprender a programar](https://miriadax.net/web/introduccion-a-la-programacion-descubre-el-lenguaje-de-la-era-digital-14-edicion-/inicio), [cursos de XP](https://www.youtube.com/playlist?list=PL8pfR1_uyqPoN-sPl7rTKas6IXy01ej8w), [un open space anual](https://ensenyar-a-programar.github.io/open-space-valencia-2019/), [hackatones](https://www.meetup.com/Aprende-a-programar-en-Valencia/events/265708509/),  [code retreats](https://www.meetup.com/Aprende-a-programar-en-Valencia/events/255737180/) y [más eventos](https://www.meetup.com/Aprende-a-programar-en-Valencia/).

Devscola está formada íntegramente por voluntarios. Nos financiamos mediante proyectos desarrollados por los propios miembros y con patrocinios de empresas que valoran nuestro trabajo.

Para que te hagas una idea, cada mes que Devscola está en funcionamiento a pleno rendimiento tiene un coste de 2.000 euros. En este momento estamos recaudando fondos para programar las actividades del primer trimestre de 2020. 
**Únete a nuestra comunidad de aprendizaje. Ayúdanos a financiarla para que nuestra actividad tenga más impacto.**



## El Run

Una de las actividades más importantes que se hacen en Devscola es lo que denominamos "Run". Es un curso que típicamente dura **3 meses** donde los miembros con más experiencia se vuelcan en la formación de personas que estan todavía aprendiendo a programar. 
El foco, más allá de aprender un lenguaje de programación en concreto, es más trasversal, dando más importancia a las **buenas prácticas (XP, TDD y Agile)** y capacitar a que todo aquel con aptitudes para la programación sea capaz de **entrar en el mercado laboral**.

### Planificación
El run consta de **3 módulos**, uno por mes. Esta pensado para una persona sin conocimientos de programación termine con unas habilidades que le permitan no solo continuar con su formación, sino también estar en condiciones de presentarse a entrevistas de trabajo
Cada persona tiene un **itinerario personalizado** en función de su nivel de autonomía. Esto quiere decir que personas con cualquier nivel de programación se pueden matricular al run y aprovecharlo al máximo.
El concepto de que todos los miembros del run sigan la misma planificación es muy potente ya que se estrechan los lazos, aumenta la motivación y se aprende más rápido.

1. 1er mes.itinerario básico: 
  Se aprende a utilizar la linea de comandos, git y se comienza con katas con el lenguage de programación elegido para el run.
  Durante este mes se imparten masterclases de temas básicos de programación. Por ejemplo arrays, funciones o entornos de desarrollo.

2. 2o mes:

3. 3er mes:  

## Otras actividades que se proponen desde Devscola

- katayunos
- curso de patrones
- curso de refactoring
- sesiones de live coding
- masterclases de aprendices de ediciones anteriores.
